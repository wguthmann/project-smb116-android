package com.example.secondlife.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.secondlife.R;
import com.example.secondlife.databinding.ActivityProductBinding;
import com.example.secondlife.viewmodels.ProductViewModel;
import com.example.secondlife.views.adapters.ProductFragmentAddapter;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class ProductActivity extends AppCompatActivity {

    private ActivityProductBinding binding;
    private ProductViewModel productViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        BottomNavigationView navView = findViewById(R.id.nav_view);

        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_profile, R.id.navigation_chat, R.id.navigation_message, R.id.navigation_mail)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);

        final ProductFragmentAddapter productAdapter = new ProductFragmentAddapter(new ProductFragmentAddapter.ProductDiff());

        productViewModel = new ViewModelProvider(this).get(ProductViewModel.class);
        productViewModel.getProduct(0);
    }
}
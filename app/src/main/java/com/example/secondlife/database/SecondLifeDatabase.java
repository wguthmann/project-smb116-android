package com.example.secondlife.database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.secondlife.dao.CityDao;
import com.example.secondlife.dao.ConversationDao;
import com.example.secondlife.dao.FeedbackDao;
import com.example.secondlife.dao.MessageDao;
import com.example.secondlife.dao.ProductDao;
import com.example.secondlife.dao.ProductGenderDao;
import com.example.secondlife.dao.ProductTypeDao;
import com.example.secondlife.dao.PropositionDao;
import com.example.secondlife.dao.SaleDao;
import com.example.secondlife.dao.UserDao;
import com.example.secondlife.models.City;
import com.example.secondlife.models.Conversation;
import com.example.secondlife.models.Feedback;
import com.example.secondlife.models.Message;
import com.example.secondlife.models.Product;
import com.example.secondlife.models.ProductGender;
import com.example.secondlife.models.ProductType;
import com.example.secondlife.models.Proposition;
import com.example.secondlife.models.Sale;
import com.example.secondlife.models.User;
import com.example.secondlife.utils.Converters;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@androidx.room.Database(entities = {City.class, Conversation.class, Feedback.class, Message.class, Product.class, ProductType.class, Proposition.class, Sale.class, User.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class SecondLifeDatabase extends RoomDatabase {

    private static volatile SecondLifeDatabase instance; //singleton instance
    private static final int NB_THREAD = 4;
    private static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NB_THREAD);
    public abstract CityDao cityDao();
    public abstract ConversationDao conversationDao();
    public abstract FeedbackDao feedbackDao();
    public abstract MessageDao messageDao();
    public abstract ProductDao productDao();
    public abstract ProductTypeDao productTypeDao();
    public abstract PropositionDao propositionDao();
    public abstract SaleDao saleDao();
    public abstract UserDao userDao();
    public abstract ProductGenderDao productGenderDao();

    public static synchronized SecondLifeDatabase getInstance(Context context){
        if(instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(), SecondLifeDatabase.class , "database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallBack)
                    .build();
        }
        return instance;
    } //synchronized : only one thread can access this method at the same time
    //fallbacktodestructivemigration : to handle versions

    private static RoomDatabase.Callback roomCallBack = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);

            //if we want to keep data throug app restarts
            //comment this part
            databaseWriteExecutor.execute(()-> {
                deleteAll();
                init();
            });
            // new PopulateDb(instance).execute();
        }
    };

    //initialize all data for Database
    private static void init() {
    }

    //Delete all data from database
    private static void deleteAll() {
    }

}

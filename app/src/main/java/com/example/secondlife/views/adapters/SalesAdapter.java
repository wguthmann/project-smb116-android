package com.example.secondlife.views.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.secondlife.models.Sale;
import com.example.secondlife.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapte l'objet Sale à l'affichage dans une liste
 */
public class SalesAdapter extends RecyclerView.Adapter<SalesAdapter.MyViewHolder>{

    private static List<Sale> salesList;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        //affiche les attributs de Sale
        public TextView SalePrice,SaleName,sellerName;
        public MyViewHolder(View itemView) {
            super(itemView);
            SalePrice = itemView.findViewById(R.id.SalePrice);
            SaleName = itemView.findViewById(R.id.SaleName);
            sellerName = itemView.findViewById(R.id.SellerName);
        }
        public void display(Sale sale){
            SalePrice.setText("Prix : "+sale.getPrice()+ " $");
            SaleName.setText("nom du jeu : null");
            sellerName.setText("vendeur : null");
            if(sale.getBuyer() != null){
                SaleName.setText("nom du jeu :" + sale.getBuyer().getUsername());
            }
            if(sale.getSaleProduct() != null){
                sellerName.setText("vendeur : "+ sale.getSaleProduct().getTitle());
            }
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public SalesAdapter(List<Sale> salesList) {
        this.salesList=salesList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public SalesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        // create a new view
        LayoutInflater v = LayoutInflater.from(parent.getContext());
        View view = v.inflate(R.layout.display_sale, parent, false);

        MyViewHolder vh = new MyViewHolder(view);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    //@Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.display(salesList.get(position));

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return salesList.size();
    }

    public static Sale getItem(int position) {
        return salesList.get(position);
    }


}

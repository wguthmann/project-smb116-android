package com.example.secondlife.views.Chat;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.secondlife.R;
import com.example.secondlife.databinding.FragmentConversationBinding;
import com.example.secondlife.models.Conversation;
import com.example.secondlife.viewmodels.Chat.ConversationViewModel;
import com.example.secondlife.views.adapters.ConversationFragmentAdapter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class ConversationFragment extends Fragment {

    private RecyclerView recyclerview_conversation;

    private ConversationFragmentAdapter adapter;

    private RecyclerView.LayoutManager layoutManager;

    private List<Conversation> convs = new ArrayList<>();

    private FragmentConversationBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_chat, container, false);
        recyclerview_conversation = root.findViewById(R.id.recyclerview_conversation);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerview_conversation.setLayoutManager(layoutManager);
        populateConversation();
        adapter = new ConversationFragmentAdapter(convs);
        recyclerview_conversation.setAdapter(adapter);

        return root;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void populateConversation() {
        Conversation conv = new Conversation(1, null, null);
        convs.add(conv);
        conv = new Conversation(2, null, null);
        convs.add(conv);
    }

}

package com.example.secondlife.views.adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.secondlife.R;
import com.example.secondlife.models.Conversation;
import com.example.secondlife.views.Chat.MessageFragment;

import java.util.List;
import java.util.Random;

public class ConversationFragmentAdapter extends RecyclerView.Adapter<ConversationFragmentAdapter.ConversationViewHolder> {

    private List<Conversation> convs;

    public ConversationFragmentAdapter(List<Conversation> conversations) {
        this.convs = conversations;
    }

    @NonNull
    @Override
    public ConversationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.chat_item, parent, false);
        return  new ConversationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ConversationViewHolder holder, int position) {
        Conversation conv = convs.get(position);

        Random random = new Random();
        int currentColor = Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256));
        holder.imgChat.setBackgroundColor(currentColor);
        int title = conv.getConversationId();
        holder.txtChat.setText("Conversation");

        holder.btnChat.setOnClickListener(view -> {
            AppCompatActivity activity = (AppCompatActivity) view.getContext();
            FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            MessageFragment messageFragment = new MessageFragment();
            ft.replace(R.id.nav_host_fragment, messageFragment);
            ft.addToBackStack(null);
            ft.commit();
        });
    }

    @Override
    public int getItemCount() {
        return convs.size();
    }

    public class ConversationViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgChat;
        private TextView txtChat;
        private Button btnChat;

        public ConversationViewHolder(@NonNull View itemView) {
            super(itemView);
            imgChat = itemView.findViewById(R.id.imgChat);
            txtChat = itemView.findViewById(R.id.txtChat);
            btnChat = itemView.findViewById(R.id.btnChat);
        }
    }
}

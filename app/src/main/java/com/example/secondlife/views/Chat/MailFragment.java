package com.example.secondlife.views.Chat;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.secondlife.R;
import com.example.secondlife.utils.JavaMailAPI;
import com.example.secondlife.utils.Utils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MailFragment extends Fragment {

    public EditText mEmail;
    public EditText mSubject;
    public EditText mMessage;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_mail, container, false);
        mEmail = root.findViewById(R.id.mailID);
        mMessage = root.findViewById(R.id.messageID);
        mSubject = root.findViewById(R.id.subjectID);

        FloatingActionButton fab = root.findViewById(R.id.fab);
        fab.setOnClickListener(view -> sendMail());

        return root;
    }

    private void sendMail() {

        String mail = mEmail.getText().toString().trim();
        String message = mMessage.getText().toString();
        String subject = mSubject.getText().toString().trim();

        try {
            //Send Mail
            JavaMailAPI javaMailAPI = new JavaMailAPI(getContext(),mail,subject,message);

            javaMailAPI.execute();
        } catch (Exception e) {
            Log.e("SendMail", e.getMessage(), e);
        }

    }
}

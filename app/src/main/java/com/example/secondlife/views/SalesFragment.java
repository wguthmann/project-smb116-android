package com.example.secondlife.views;

import android.app.Application;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.secondlife.R;
import com.example.secondlife.databinding.FragmentHomeBinding;
import com.example.secondlife.databinding.FragmentSalesBinding;
import com.example.secondlife.models.Product;
import com.example.secondlife.models.Sale;
import com.example.secondlife.models.User;
import com.example.secondlife.repositories.SaleRepository;
import com.example.secondlife.viewmodels.DashboardViewModel;
import com.example.secondlife.viewmodels.Sale.SaleViewModel;
import com.example.secondlife.viewmodels.UserViewModel;
import com.example.secondlife.views.adapters.SalesAdapter;

import java.util.ArrayList;
import java.util.List;

public class SalesFragment extends Fragment {

    private FragmentSalesBinding binding;
    private DashboardViewModel dashboardViewModel;

    private UserViewModel userViewModel = new UserViewModel();

    private static List<Sale> salesList = new ArrayList<>();

    private RecyclerView rvSales;

    private RecyclerView.LayoutManager layoutManager;

    private static RecyclerView.Adapter salesAdapter;

    public SalesFragment(){
        //empty constructor
    }

    public static void clearFragment(){salesList.clear();}

    /**
     * Crée une nouvelle sale et l'ajoute à la liste saleList
     */
    public static void addSale(float price, User buyer, User seller, Product saleProduct){
        salesList.add(new Sale(price, buyer,seller,saleProduct));
        salesAdapter.notifyDataSetChanged();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentSalesBinding.inflate(getLayoutInflater());
        dashboardViewModel =
                new ViewModelProvider(this).get(DashboardViewModel.class);
        View root = binding.getRoot();

        rvSales = binding.rvSales;
        rvSales.setHasFixedSize(true);

        salesList = new ArrayList<>();

        layoutManager = new LinearLayoutManager(getContext());
        rvSales.setLayoutManager(layoutManager);

        salesAdapter = new SalesAdapter(salesList);
        rvSales.setAdapter(salesAdapter);

        //salesList.add(new Sale(29,null,null,null));
        //salesList.add(new Sale(5,null,null,null));

        SaleRepository repo = new SaleRepository((Application)getContext().getApplicationContext());
        //salesList=repo.getAllSales().getValue();

        SaleViewModel.getAllSales(getContext());

        //configureOnClickRecyclerView();

        //rvSales = root.


        return root;
    }


}
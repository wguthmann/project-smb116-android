package com.example.secondlife.views.adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.example.secondlife.R;
import com.example.secondlife.models.Conversation;
import com.example.secondlife.models.Message;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MessageFragmentAdapter extends RecyclerView.Adapter<MessageFragmentAdapter.ChatViewHolder> {

    private static List<Message> messageList;


    public MessageFragmentAdapter(List<Message> messages) {
        this.messageList = messages;
    }

    @NonNull
    @Override
    public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.message_item, parent, false);
        return new ChatViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageFragmentAdapter.ChatViewHolder holder, int position) {
        Message message = messageList.get(position);

        String content = message.getText();
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    public class ChatViewHolder extends RecyclerView.ViewHolder {

        private TextView text;

        public ChatViewHolder(@NonNull View itemView) {
            super(itemView);
            text = itemView.findViewById(R.id.txtMessage);
        }

    }
}

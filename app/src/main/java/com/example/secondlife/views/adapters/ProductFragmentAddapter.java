package com.example.secondlife.views.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.example.secondlife.R;
import com.example.secondlife.models.Product;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class ProductFragmentAddapter extends ListAdapter<Product, ProductFragmentAddapter.ViewHolder> {

    public ProductFragmentAddapter(@NonNull DiffUtil.ItemCallback<Product> callback){
        super(callback);
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return ViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Product product = getItem(position);
        holder.bind(product);
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private View productItemView;
        private TextView title;
        private TextView author;
        private TextView description;

        public ViewHolder(View itemView) {
            super(itemView);
            findProductItemView();
            findTitle();
            findAuthor();
            findDescription();
            itemView.setOnClickListener(this);
        }

        public void bind(Product product){
            title.setText(product.title);
            author.setText(product.ownerUser.username);
            description.setText(product.description);
        }

        public static ViewHolder create(ViewGroup parent){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_row_item, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onClick(View v) {

        }

        public TextView getTitle(){
            return title;
        }
        public TextView getAuthor() {
            return author;
        }
        public TextView getDescription() {
            return description;
        }

        private void findTitle() {
            title = itemView.findViewById(R.id.ProducTitle);
        }
        private void findAuthor() {
            author = itemView.findViewById(R.id.ProductAuthor);
        }
        private void findProductItemView(){
            productItemView = itemView.findViewById(R.id.ProductInfoDisplay);
        }
        private void findDescription() {
            description = itemView.findViewById(R.id.ProductDescription);
        }
    }

    public static class ProductDiff extends DiffUtil.ItemCallback<Product>{

        @Override
        public boolean areItemsTheSame(@NonNull Product oldItem, @NonNull Product newItem) {
            return oldItem.productId == newItem.productId;
        }

        @Override
        public boolean areContentsTheSame(@NonNull Product oldItem, @NonNull Product newItem) {
            return oldItem.description.equals(newItem.description);
        }
    }
}

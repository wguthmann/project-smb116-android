package com.example.secondlife.views.User;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.secondlife.MainActivity;
import com.example.secondlife.R;
import com.example.secondlife.models.City;
import com.example.secondlife.models.User;
import com.example.secondlife.network.IUserService;
import com.example.secondlife.network.Retrofit;
import com.example.secondlife.viewmodels.UserViewModel;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    IUserService service = Retrofit.getInstance().create(IUserService.class);
    private UserViewModel userViewModel = new UserViewModel();

    private TextView title;
    private EditText usernameInput;
    private EditText emailAddressInput;
    private EditText cityInput;
    private EditText passwordInput;
    private EditText passwordConfirmationInput;
    private Button registerButton;
    private Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_register);
//        initDatePicker();

        title = findViewById(R.id.login_fragment_title);
        usernameInput = findViewById(R.id.login_fragment_username);
        emailAddressInput = findViewById(R.id.register_fragment_mail);
//        dateButton = findViewById(R.id.register_fragment_date_picker_button);
        cityInput = findViewById(R.id.register_fragment_city);
        passwordInput = findViewById(R.id.login_fragment_password);
        passwordConfirmationInput = findViewById(R.id.register_fragment_password_confirmation);
        registerButton = findViewById(R.id.login_fragment_btn_register);
        loginButton = findViewById(R.id.login_fragment_btn_login);

        registerButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String username = usernameInput.getText().toString();
                String emailAddress = emailAddressInput.getText().toString();
//                Date birthdate = getDateFromDatePicker(datePickerDialog.getDatePicker());
                String city = cityInput.getText().toString();
                String password = passwordInput.getText().toString();
                String passwordConfirmation = passwordConfirmationInput.getText().toString();


                if (isFormConsistent(username, emailAddress,
                        city, password, passwordConfirmation)) {

                    userViewModel.createUser(username, emailAddress, password, RegisterActivity.this);
                }
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
            }
        });
    }


    private boolean isFormConsistent(String username, String emailAddress,
                                     String city, String password, String passwordConfirmation) {
        Pattern emailPattern = Pattern.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\[\\x01-\\x09\\x0b\\x0c\\x0e\\-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");
        Matcher emailRegex = emailPattern.matcher(emailAddress);
        Pattern passwordPattern = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$");
        Matcher passwordRegex = passwordPattern.matcher(password);

        if (username.length() <= 0) {
            Toast.makeText(this, "Veuillez renseigner votre nom d'utilisateur", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!emailRegex.matches()) {
            Toast.makeText(this, "Veuillez entrer une adresse mail valide", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!passwordRegex.matches()) {
            Toast.makeText(this, "Le mot de passe doit contenir au minimum :\n- 8 caractères\n- 1 lettre majuscule\n- 1 lettre minuscule\n- 1 chiffre\n- 1 caractère spécial", Toast.LENGTH_LONG).show();
            return false;
        }
        if (!password.contentEquals(passwordConfirmation)) {
            Toast.makeText(this, "Les mots de passe ne correspondent pas", Toast.LENGTH_SHORT).show();
            System.out.println("\n" + password + "\n" + passwordConfirmation);
            return false;
        }

        return true;
    }
}
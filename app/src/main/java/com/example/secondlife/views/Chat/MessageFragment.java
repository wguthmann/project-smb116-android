package com.example.secondlife.views.Chat;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.secondlife.R;
import com.example.secondlife.databinding.FragmentChatBinding;
import com.example.secondlife.databinding.FragmentMessageBinding;
import com.example.secondlife.models.Conversation;
import com.example.secondlife.models.Message;
import com.example.secondlife.views.adapters.ConversationFragmentAdapter;
import com.example.secondlife.views.adapters.MessageFragmentAdapter;

import java.util.ArrayList;
import java.util.List;


public class MessageFragment extends Fragment {

    private RecyclerView recyclerview_message;

    private FragmentMessageBinding binding;

    private MessageFragmentAdapter adapter;

    private RecyclerView.LayoutManager layoutManager;


    private List<Message> messages = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_message, container, false);
        recyclerview_message =  root.findViewById(R.id.messages_recyclerview);;
        layoutManager = new LinearLayoutManager(getContext());
        recyclerview_message.setLayoutManager(layoutManager);
        populateMessage();
        adapter = new MessageFragmentAdapter(messages);
        recyclerview_message.setAdapter(adapter);

        return root;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    public void populateMessage() {
        Message mess = new Message(1, "Salut ça va ?", null, null, null, null, null);
        messages.add(mess);
        mess = new Message(2, "oui et toi ?", null, null, null, null, null);
        messages.add(mess);
    }
}

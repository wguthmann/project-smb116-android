package com.example.secondlife.views.User;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.secondlife.R;
import com.example.secondlife.databinding.FragmentProfileBinding;
import com.example.secondlife.viewmodels.UserViewModel;
//import com.example.secondlife.viewmodels.Chat.MessageViewModel;

public class ProfileFragment extends Fragment {
    private static FragmentProfileBinding binding;
    private UserViewModel userViewModel = new UserViewModel();
    //private MessageViewModel messageViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentProfileBinding.inflate(getLayoutInflater());
//        messageViewModel =
//                new ViewModelProvider(this).get(MessageViewModel.class);
        View root = binding.getRoot();

        // TODO: Récupérer le user avec JWT
        userViewModel.profile(1, getContext());

        binding.profileFragmentBtnMyGames.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println(binding.profileFragmentUsername.getText().toString());
                Toast toast = Toast.makeText(v.getContext(), binding.profileFragmentUsername.getText().toString(), Toast.LENGTH_SHORT);
                toast.show();

                Fragment myGamesFragment = new myGamesFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.constraintProfile, myGamesFragment ); // give your fragment container id in first parameter
                transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
                transaction.commit();

                System.out.println("commit done");
            }
        });

        return root;
    }

    public static void CompleteUserInfos(String username, String mail){
        binding.profileFragmentUsername.setText(username);
        binding.profileFragmentMail.setText(mail);
    }
}

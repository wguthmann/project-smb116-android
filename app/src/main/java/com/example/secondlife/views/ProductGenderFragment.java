package com.example.secondlife.views;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.secondlife.databinding.FragmentProductGenderBinding;

public class ProductGenderFragment extends Fragment {

    private FragmentProductGenderBinding binding;
    private RecyclerView recyclerView;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentProductGenderBinding.inflate(getLayoutInflater(), container, false);
        recyclerView = binding.productGenderRecyclerview;
        return binding.getRoot();
    }


}
package com.example.secondlife.views.User;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.secondlife.MainActivity;
import com.example.secondlife.R;
import com.example.secondlife.viewmodels.UserViewModel;

public class LoginActivity extends AppCompatActivity {

    private UserViewModel userViewModel = new UserViewModel();

    private TextView title;
    private EditText usernameInput;
    private EditText passwordInput;
    private Button loginButton;
    private Button registerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_login);

        title = findViewById(R.id.login_fragment_title);
        usernameInput = findViewById(R.id.login_fragment_username);
        passwordInput = findViewById(R.id.login_fragment_password);
        loginButton = findViewById(R.id.login_fragment_btn_login);
        registerButton = findViewById(R.id.login_fragment_btn_register);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String password = passwordInput.getText().toString();
                String username = usernameInput.getText().toString();

                userViewModel.login(password, username, LoginActivity.this);
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });
    }
}
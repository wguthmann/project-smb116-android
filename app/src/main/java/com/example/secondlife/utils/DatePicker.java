package com.example.secondlife.utils;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.widget.Button;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DatePicker {

    private String getTodaysDate() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        month = month + 1;
        int day = cal.get(Calendar.DAY_OF_MONTH);
        return makeDateString(day, month, year);
    }


    private void initDatePicker(Button dateButton, DatePickerDialog datePickerDialog, Context context) {
        DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(android.widget.DatePicker view, int year, int month, int day) {
                month = month + 1;
                String date = makeDateString(day, month, year);
                dateButton.setText(date);
            }
        };

        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        int style = AlertDialog.THEME_HOLO_LIGHT;

        Locale.setDefault(Locale.FRANCE);
        datePickerDialog = new DatePickerDialog(context, style, dateSetListener, year, month, day);
//        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
    }


    private String makeDateString(int day, int month, int year) {
//        return day + "/" + month + "/" + year;
        return day + " " + getMonthFormat(month) + " " + year;
    }


    private String getMonthFormat(int month) {
        if (month == 1)
            return "Janvier";
        if (month == 2)
            return "Février";
        if (month == 3)
            return "Mars";
        if (month == 4)
            return "Avril";
        if (month == 5)
            return "Mai";
        if (month == 6)
            return "Juin";
        if (month == 7)
            return "Juillet";
        if (month == 8)
            return "Août";
        if (month == 9)
            return "Septembre";
        if (month == 10)
            return "Octobre";
        if (month == 11)
            return "Novembre";
        if (month == 12)
            return "Décembre";

        //default should never happen
        return "Janvier";
    }


//    public static Date getDateFromDatePicker(DatePicker datePicker) {
//        int day = datePicker.getDayOfMonth();
//        int month = datePicker.getMonth();
//        int year = datePicker.getYear();
//
//        Calendar calendar = Calendar.getInstance();
//        calendar.set(year, month, day);
//
//        return calendar.getTime();
//    }
}

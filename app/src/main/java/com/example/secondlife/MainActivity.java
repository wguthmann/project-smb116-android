package com.example.secondlife;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.secondlife.databinding.ActivityMainBinding;
import com.example.secondlife.viewmodels.HomeViewModel;
import com.example.secondlife.views.adapters.ProductFragmentAddapter;
import com.example.secondlife.utils.JavaMailAPI;
import com.example.secondlife.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;
    private HomeViewModel homeViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_profile, R.id.navigation_chat, R.id.navigation_message, R.id.navigation_mail)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);

        //setup RecyclerView
//        RecyclerView productRecyclerView = findViewById(R.id.productRecyclerview);
        final ProductFragmentAddapter productAdapter = new ProductFragmentAddapter(new ProductFragmentAddapter.ProductDiff());
//        productRecyclerView.setAdapter(productAdapter);
//        productRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        homeViewModel.getAllProducts().observe(this, productAdapter::submitList);
    }
}
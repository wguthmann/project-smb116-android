package com.example.secondlife.viewmodels;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.secondlife.models.Product;
import com.example.secondlife.network.IProductService;
import com.example.secondlife.network.Retrofit;
import com.example.secondlife.repositories.ProductRepository;
import com.example.secondlife.views.adapters.ProductFragmentAddapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeViewModel extends AndroidViewModel {

    IProductService service = Retrofit.getInstance().create(IProductService.class);

    private ProductRepository repository;
    private final LiveData<List<Product>> products;

    public HomeViewModel(Application application) {
        super(application);
        repository = new ProductRepository(application);
        products = repository.getAllProducts();

    }

    public LiveData<List<Product>> getAllProducts(){
        return products;
    }

    public void insert(Product product){
        repository.insert(product);
    }

    private void getProducts() {
        new Thread(() -> {
            this.service.findAll().enqueue(new Callback<List<Product>>() {
                @Override
                public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                    Log.d("getProducts", String.valueOf(response));
                    Log.d("getProducts", String.valueOf(response.body()));
                    List<Product> listSales = response.body();
                }

                @Override
                public void onFailure(Call<List<Product>> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        }).start();
    }

    private void getPreferences(){
        new Thread(()->{
            this.service.getPreferences().enqueue(new Callback<List<Product>>() {
                  @Override
                  public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                    List<Product> products = response.body();
                    ProductFragmentAddapter fragmentAddapter = new ProductFragmentAddapter(new ProductFragmentAddapter.ProductDiff());
                  }
                  @Override
                  public void onFailure(Call<List<Product>> call, Throwable t) {
                      t.printStackTrace();
                  }
            });
        });
    }
}
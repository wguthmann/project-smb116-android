package com.example.secondlife.viewmodels;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.secondlife.MainActivity;
import com.example.secondlife.models.User;
import com.example.secondlife.network.IUserService;
import com.example.secondlife.network.Retrofit;
import com.example.secondlife.views.User.ProfileFragment;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserViewModel {
    IUserService service = Retrofit.getInstance().create(IUserService.class);

    public void login(String password, String username, Context context) {

        User user = new User(password, username);

        Call<User> call = service.validateCredentials(user);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    User userResponse = response.body();
                    if (userResponse == null) {
                        Toast.makeText(context, "Identifiants incorrects", Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        context.startActivity(new Intent(context, MainActivity.class));
                    }
                }

                else {
                    Toast.makeText(context, "Identifiants incorrects", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(context, "Connexion impossible", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void createUser(String username, String emailAddress, String password, Context context) {

        User user = new User(username, emailAddress, password);

        Call<User> call = service.createUser(user);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    User userResponse = response.body();
                    if (userResponse == null) {
                        Toast.makeText(context, "Erreur", Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        context.startActivity(new Intent(context, MainActivity.class));
                    }
                }

                else {
                    Toast.makeText(context, "Le nom d'utilisateur ou le mot de passe est déjà utilisé", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(context, "Erreur serveur", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void profile(int id, Context context) {

        Call<User> call = service.findOne(id);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    User userResponse = response.body();
                    if (userResponse == null) {
                        Toast.makeText(context, "Identifiants incorrects", Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        ProfileFragment.CompleteUserInfos(userResponse.username, userResponse.emailAddress);
                    }
                }

                else {
                    Toast.makeText(context, "Identifiants incorrects", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(context, "Connexion impossible", Toast.LENGTH_SHORT).show();
            }
        });
    }
}

package com.example.secondlife.viewmodels;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.secondlife.models.Product;
import com.example.secondlife.models.ProductGender;
import com.example.secondlife.network.IProductGenderService;
import com.example.secondlife.network.IProductService;
import com.example.secondlife.network.Retrofit;
import com.example.secondlife.repositories.ProductGenderRepository;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProductGenderViewModel extends AndroidViewModel {

    private ProductGenderRepository repository;
    private final LiveData<List<ProductGender>> productGenders;
    IProductGenderService service = Retrofit.getInstance().create(IProductGenderService.class);

    public ProductGenderViewModel(@NonNull Application application) {
        super(application);
        repository = new ProductGenderRepository(application);
        productGenders = repository.getAllProductGenders();
    }

    private void getProductGenders() {
        new Thread(() -> {
            this.service.findAll().enqueue(new Callback<List<ProductGender>>() {
                @Override
                public void onResponse(Call<List<ProductGender>> call, Response<List<ProductGender>> response) {
                    Log.d("getProducts", String.valueOf(response));
                    Log.d("getProducts", String.valueOf(response.body()));
                    List<ProductGender> listSales = response.body();
                }

                @Override
                public void onFailure(Call<List<ProductGender>> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        }).start();
    }
}

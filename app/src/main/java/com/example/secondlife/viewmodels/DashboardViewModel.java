package com.example.secondlife.viewmodels;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.secondlife.models.Product;
import com.example.secondlife.models.Sale;
import com.example.secondlife.network.IProductService;
import com.example.secondlife.network.ISaleService;
import com.example.secondlife.network.Retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardViewModel extends ViewModel {

    IProductService service = Retrofit.getInstance().create(IProductService.class);

    private MutableLiveData<String> mText;

    public DashboardViewModel() {
        getProducts();
        mText = new MutableLiveData<>();
        mText.setValue("This is dashboard fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }

    private void getProducts() {
        new Thread(() -> {
            this.service.findAll().enqueue(new Callback<List<Product>>() {
                @Override
                public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                    Log.d("getProducts", String.valueOf(response));
                    Log.d("getProducts", String.valueOf(response.body()));
                    List<Product> listSales = response.body();
                }

                @Override
                public void onFailure(Call<List<Product>> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        }).start();
    }


}
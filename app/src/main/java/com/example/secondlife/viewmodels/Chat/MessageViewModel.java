package com.example.secondlife.viewmodels;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.secondlife.models.Conversation;
import com.example.secondlife.models.Product;
import com.example.secondlife.models.Sale;
import com.example.secondlife.models.User;
import com.example.secondlife.network.IConversationService;
import com.example.secondlife.network.ISaleService;
import com.example.secondlife.network.Retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessageViewModel extends ViewModel {

    IConversationService service = Retrofit.getInstance().create(IConversationService.class);

    private MutableLiveData<List<Conversation>> liveDataConversations;
    private List<Conversation> responseData;

    public MessageViewModel() {
        getConversations();
        liveDataConversations = new MutableLiveData<>();
        liveDataConversations.setValue(responseData);
    }

    public void getConversations() {

        new Thread(() -> {
            this.service.findAll().enqueue(new Callback<List<Conversation>>() {
                @Override
                public void onResponse(Call<List<Conversation>> call, Response<List<Conversation>> response) {
                    Log.d("getConversations", String.valueOf(response));
                    Log.d("getConversations", String.valueOf(response.body()));
                    responseData = response.body();
                }

                @Override
                public void onFailure(Call<List<Conversation>> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        }).start();

    }


    public void postSales() {
//        Sale sale = new Sale("10.0", );
//        new Thread(() -> {
//            this.service.add().enqueue(new Callback<List<Sale>>() {
//                @Override
//                public void onResponse(Call<List<Sale>> call, Response<List<Sale>> response) {
//                    Log.d("getSales", String.valueOf(response));
//                    Log.d("getSales", String.valueOf(response.body()));
//                    liveDataSales.postValue(response.body());
//                }
//
//                @Override
//                public void onFailure(Call<List<Sale>> call, Throwable t) {
//                    t.printStackTrace();
//                }
//            });
//
//        }).start();
    }

    public void patchSales() {
    }

    public void deleteSales() {
    }

    /*public void patchSales() {
        new Thread(() -> {
            this.service.findAll().enqueue(new Callback<patch>() {
                @Override
                public void onResponse(Call<patch> call, Response<patch> response) {
                    Log.d("getSales", String.valueOf(response));
                    Log.d("getSales", String.valueOf(response.body()));
                    liveDataSales.postValue(response.body());
                }

                @Override
                public void onFailure(Call<patch> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        }).start();
    }

    public void deleteSales() {
        new Thread(() -> {
            this.service.findAll().enqueue(new Callback<delete>() {
                @Override
                public void onResponse(Call<delete> call, Response<delete> response) {
                    Log.d("getSales", String.valueOf(response));
                    Log.d("getSales", String.valueOf(response.body()));
                    liveDataSales.postValue(response.body());
                }

                @Override
                public void onFailure(Call<delete> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        }).start();
    }*/
}


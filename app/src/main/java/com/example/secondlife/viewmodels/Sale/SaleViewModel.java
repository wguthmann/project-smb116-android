package com.example.secondlife.viewmodels.Sale;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.example.secondlife.MainActivity;
import com.example.secondlife.models.Sale;
import com.example.secondlife.models.User;
import com.example.secondlife.network.ISaleService;
import com.example.secondlife.network.IUserService;
import com.example.secondlife.network.Retrofit;
import com.example.secondlife.repositories.SaleRepository;
import com.example.secondlife.views.SalesFragment;
import com.example.secondlife.views.adapters.SalesAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SaleViewModel {

    private static ISaleService service = Retrofit.getInstance().create(ISaleService.class);


    public static void getAllSales(Context context) {

        //SaleRepository repo = new SaleRepository((Application)context.getApplicationContext());
        Call<List<Sale>> call = service.findAll();

        call.enqueue(new Callback<List<Sale>>() {
            @Override
            public void onResponse(Call<List<Sale>> call, Response<List<Sale>> response) {
                if (response.isSuccessful()) {

                    //List<Sale> salesList = new ArrayList<Sale>();

                    for(Sale sale : response.body()){
                        //repo.insert(sale);
                        //salesList.add(sale);
                        SalesFragment.addSale(sale.price, sale.buyer, sale.seller, sale.saleProduct);
                    }
                }

                else {
                    Toast.makeText(context, "response unsuccessful", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Sale>> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(context, "Connexion impossible", Toast.LENGTH_SHORT).show();
            }
        });
    }
}

package com.example.secondlife.viewmodels.Chat;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.secondlife.models.Conversation;
import com.example.secondlife.network.IConversationService;
import com.example.secondlife.network.Retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConversationViewModel extends ViewModel {

    IConversationService service = Retrofit.getInstance().create(IConversationService.class);

    private MutableLiveData<List<Conversation>> liveDataConversations;
    private List<Conversation> responseData;

    public ConversationViewModel() {
        getConversations();
        liveDataConversations = new MutableLiveData<>();
        liveDataConversations.setValue(responseData);
    }

    public void getConversations() {

        new Thread(() -> {
            this.service.findAll().enqueue(new Callback<List<Conversation>>() {
                @Override
                public void onResponse(Call<List<Conversation>> call, Response<List<Conversation>> response) {
                    Log.d("getConversations", String.valueOf(response));
                    Log.d("getConversations", String.valueOf(response.body()));
                    responseData = response.body();
                }

                @Override
                public void onFailure(Call<List<Conversation>> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        }).start();

    }
}

package com.example.secondlife.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.example.secondlife.models.Product;
import com.example.secondlife.network.IProductService;
import com.example.secondlife.network.Retrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductViewModel extends AndroidViewModel {

    IProductService service = Retrofit.getInstance().create(IProductService.class);

    public ProductViewModel(@NonNull Application application) {
        super(application);
    }

    public void getProduct(int id){
        new Thread(()->{
            service.findOne(id).enqueue(new Callback<Product>() {
                @Override
                public void onResponse(Call<Product> call, Response<Product> response) {
                    Product p = response.body();
                }

                @Override
                public void onFailure(Call<Product> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        });
    }
}

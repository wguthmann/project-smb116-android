
package com.example.secondlife.network;


import com.example.secondlife.models.ProductGender;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface IProductGenderService {

    @GET("api/productgender")
    Call<List<ProductGender>> findAll();

    @GET("api/productgender/{id}")
    Call<ProductGender> findOne(@Path("id") int id);

    @PATCH("api/productgender/{id}")
    Call<ProductGender> patch(@Path("id") int id, @Body ProductGender jsonPatch);

    @PUT("api/productgender")
    Call<ProductGender> add(@Body ProductGender newSale);

    @DELETE("api/productgender/{id}")
    Call<ProductGender> delete(@Path("id") int id);
}

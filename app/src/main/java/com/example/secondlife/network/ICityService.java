package com.example.secondlife.network;

import com.example.secondlife.models.City;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ICityService {

    @GET("api/city")
    Call<List<City>> findAll();

    @GET("api/city/{id}")
    Call<City> findOne(@Path("id") int id);

    @PATCH("api/city/{id}")
    Call<City> patch(@Path("id") int id, @Body City jsonPatch);

    @PUT("api/city")
    Call<City> add(@Body City newCity);

    @DELETE("api/city/{id}")
    Call<City> delete(@Path("id") int id);
}

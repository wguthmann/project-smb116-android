package com.example.secondlife.network;

import com.example.secondlife.models.Message;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface IMessageService {

    @GET("api/message")
    Call<Message> findAll();

    @GET("api/message/{id}")
    Call<Message> findOne(@Path("id") int id);

    @PATCH("api/message/{id}")
    Call<Message> patch(@Path("id") int id, @Body Message jsonPatch);

    @PUT("api/message")
    Call<Message> add(@Body Message newSale);

    @DELETE("api/message/{id}")
    Call<Message> delete(@Path("id") int id);
}

package com.example.secondlife.network;

import com.example.secondlife.models.Conversation;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface IConversationService {
    @GET("api/conversation")
    Call<List<Conversation>> findAll();

    @GET("api/conversation/{id}")
    Call<Conversation> findOne(@Path("id") int id);

    @PATCH("api/conversation/{id}")
    Call<Conversation> patch(@Path("id") int id, @Body Conversation jsonPatch);

    @PUT("api/conversation")
    Call<Conversation> add(@Body Conversation newSale);

    @DELETE("api/conversation/{id}")
    Call<Conversation> delete(@Path("id") int id);
}

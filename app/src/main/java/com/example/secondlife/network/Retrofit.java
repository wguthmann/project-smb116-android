package com.example.secondlife.network;

import com.example.secondlife.OkHttpClass;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.converter.gson.GsonConverterFactory;

public class Retrofit {

    private static retrofit2.Retrofit retrofit;

//    private final static String BASE_URL = "http://10.0.2.2:5000/";
    private final static String BASE_URL = "http://92.148.233.171:5000/";

    public static retrofit2.Retrofit getInstance() {
        if(retrofit == null) {
            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                    .create();
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(OkHttpClass.getUnsafeOkHttpClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit;
    }
}
package com.example.secondlife.network;

import com.example.secondlife.models.Sale;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ISaleService {

    @GET("api/sale/")
    Call<List<Sale>> findAll();

    @GET("api/sale/{id}")
    Call<Sale> findOne(@Path("id") int id);

    @PATCH("api/sale/{id}")
    Call<Sale> patch(@Path("id") int id, @Body Sale jsonPatch);

    @PUT("api/sale")
    Call<Sale> add(@Body Sale newSale);

    @DELETE("api/sale/{id}")
    Call<Sale> delete(@Path("id") int id);
}

package com.example.secondlife.network;

import com.example.secondlife.models.ProductType;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface IProductTypeService {

    @GET("api/producttype")
    Call<List<ProductType>> findAll();

    @GET("api/producttype/{id}")
    Call<ProductType> findOne(@Path("id") int id);

    @PATCH("api/producttype/{id}")
    Call<ProductType> patch(@Path("id") int id, @Body ProductType jsonPatch);

    @PUT("api/producttype")
    Call<ProductType> add(@Body ProductType newSale);

    @DELETE("api/producttype/{id}")
    Call<ProductType> delete(@Path("id") int id);
}

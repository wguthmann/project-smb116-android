package com.example.secondlife.network;

import com.example.secondlife.models.Proposition;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface IPropositionService {

    @GET("api/proposition")
    Call<List<Proposition>> findAll();

    @GET("api/proposition/{id}")
    Call<Proposition> findOne(@Path("id") int id);

    @PATCH("api/proposition/{id}")
    Call<Proposition> patch(@Path("id") int id, @Body Proposition jsonPatch);

    @PUT("api/proposition")
    Call<Proposition> add(@Body Proposition newSale);

    @DELETE("api/proposition/{id}")
    Call<Proposition> delete(@Path("id") int id);
}

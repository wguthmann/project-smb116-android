package com.example.secondlife.network;


import com.example.secondlife.models.Feedback;
import com.example.secondlife.models.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface IUserService {

    @GET("api/user")
    Call<List<User>> getAllUsers();

    @GET("api/user/{id}")
    Call<User> findOne(@Path("id") int id);

    @PATCH("api/user/{id}")
    Call<User> patch(@Path("id") int id, @Body User jsonPatch);

    @POST("api/user")
    Call<User> createUser(@Body User newUser);

    @DELETE("api/user/{id}")
    Call<User> delete(@Path("id") int id);

    @GET("api/user/{id}/feedback")
    Call<List<Feedback>> findFeedbacks(@Path("id") int id);

//    @FormUrlEncoded
//    @POST("api/user/validatecredentials")
//    Call<ResponseBody> validateCredentials(@Field("username") String username,
//                                           @Field("password") String password);
    @POST("api/user/validatecredentials")
    Call<User> validateCredentials(@Body User user);
//    Call<JsonObject> validateCredentials(@Body JsonObject jobject);
}

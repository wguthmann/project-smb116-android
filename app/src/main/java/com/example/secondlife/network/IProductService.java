package com.example.secondlife.network;

import com.example.secondlife.models.Product;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface IProductService {

    @GET("api/product")
    Call<List<Product>> findAll();

    @GET("api/product/{id}")
    Call<Product> findOne(@Path("id") int id);

    @PATCH("api/product/{id}")
    Call<Product> patch(@Path("id") int id, @Body Product jsonPatch);

    @PUT("api/product")
    Call<Product> add(@Body Product newSale);

    @DELETE("api/product/{id}")
    Call<Product> delete(@Path("id") int id);

    @GET("api/preferences")
    Call<List<Product>> getPreferences();
}

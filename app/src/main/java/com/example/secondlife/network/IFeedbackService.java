package com.example.secondlife.network;


import com.example.secondlife.models.Feedback;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface IFeedbackService {

    @GET("api/feedback")
    Call<Feedback> findAll();

    @GET("api/feedback/{id}")
    Call<Feedback> findOne(@Path("id") int id);

    @PATCH("api/feedback/{id}")
    Call<Feedback> patch(@Path("id") int id, @Body Feedback jsonPatch);

    @PUT("api/feedback")
    Call<Feedback> add(@Body Feedback newSale);

    @DELETE("api/feedback/{id}")
    Call<Feedback> delete(@Path("id") int id);
}

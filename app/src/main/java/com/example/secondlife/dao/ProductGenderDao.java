package com.example.secondlife.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.secondlife.models.Product;
import com.example.secondlife.models.ProductGender;

import java.util.List;

@Dao
public interface ProductGenderDao {

//    @Query("SELECT * FROM productgender")
//    LiveData<List<ProductGender>> getAll();
//
//    @Query("SELECT * FROM productGender WHERE productGenderId = :id ")
//    ProductGender findById(int id);
//
//    @Query("SELECT * FROM productGender WHERE title LIKE :title LIMIT 1")
//    ProductGender findByTitle(String title);
//
//    @Insert
//    void insertAll(ProductGender... productGenders);
//
//    @Insert
//    void Insert(ProductGender productGender);
//
//    @Update
//    void Update(ProductGender product);
//
//    @Query("DELETE FROM ProductGender")
//    void DeleteAllProductGenders();
//
//    @Delete
//    void Delete(ProductGender productGender);
}

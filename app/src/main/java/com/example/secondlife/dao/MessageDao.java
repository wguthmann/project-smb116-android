package com.example.secondlife.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.secondlife.models.City;
import com.example.secondlife.models.Message;

import java.util.List;

@Dao
public interface MessageDao {

    @Query("SELECT * FROM message")
    LiveData<List<Message>> getAll();

    @Query("SELECT * FROM message WHERE messageId = :id ")
    Message findById(int id);

    @Insert
    void insertAll(Message... messages);

    @Insert
    void Insert(Message city);

    @Update
        //(onConflict = OnConflictStrategy.REPLACE)
    void Update(Message city);

    @Query("DELETE FROM MESSAGE")
    void DeleteAllMessages();

    @Delete
    void Delete(Message message);

}

package com.example.secondlife.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.secondlife.models.City;
import com.example.secondlife.models.Conversation;

import java.util.List;

@Dao
public interface ConversationDao {

    @Query("SELECT * FROM conversation")
    LiveData<List<Conversation>> getAll();

    @Query("SELECT * FROM conversation WHERE conversationId = :id ")
    Conversation findById(int id);

    @Insert
    void insertAll(Conversation... conversations);

    @Insert
    void Insert(Conversation conversation);

    @Update
        //(onConflict = OnConflictStrategy.REPLACE)
    void Update(Conversation conversation);

    @Query("DELETE FROM CONVERSATION")
    void DeleteAllConversations();

    @Delete
    void Delete(Conversation conversation);

}

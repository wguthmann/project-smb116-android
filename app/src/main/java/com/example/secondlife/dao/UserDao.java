package com.example.secondlife.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.secondlife.models.City;
import com.example.secondlife.models.Proposition;
import com.example.secondlife.models.User;

import java.util.List;

@Dao
public interface UserDao {

    @Query("SELECT * FROM user")
    LiveData<List<User>> getAll();

//    @Query("SELECT * FROM user WHERE city_id = :id ")
//    Proposition findById(int id);

    @Query("SELECT * FROM user WHERE username LIKE :username LIMIT 1")
    User findByName(String username);

    @Insert
    void insertAll(User... users);

    @Insert
    void Insert(User user);

    @Update
    void Update(User user);

    @Delete
    void Delete(User user);

    @Query("DELETE FROM USER")
    void DeleteAllUsers();
}

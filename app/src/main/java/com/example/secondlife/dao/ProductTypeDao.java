package com.example.secondlife.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.secondlife.models.City;
import com.example.secondlife.models.ProductType;

import java.util.List;

@Dao
public interface ProductTypeDao {

    @Query("SELECT * FROM producttype")
    LiveData<List<ProductType>> getAll();

    @Query("SELECT * FROM producttype WHERE title LIKE :title LIMIT 1")
    ProductType findByTitle(String title);

    @Query("SELECT * FROM producttype WHERE id = :id ")
    ProductType findById(int id);

    @Insert
    void insertAll(ProductType... productTypes);

    @Insert
    void Insert(ProductType productType);

    @Update
        //(onConflict = OnConflictStrategy.REPLACE)
    void Update(ProductType productType);

    @Delete
    void Delete(ProductType productType);

    @Query("DELETE FROM PRODUCTTYPE")
    void DeleteAllProductTypes();

}

package com.example.secondlife.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.secondlife.models.City;
import com.example.secondlife.models.Sale;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface SaleDao {

    @Query("SELECT * FROM sale")
    LiveData<List<Sale>> getAll();

    @Query("SELECT * FROM sale WHERE id = :id ")
    Sale findById(int id);

    @Insert
    void insertAll(Sale... sales);

    @Insert
    void Insert(Sale sale);

    @Update
        //(onConflict = OnConflictStrategy.REPLACE)
    void Update(Sale sale);

    @Delete
    void Delete(Sale sale);

    @Query("DELETE FROM SALE")
    void DeleteAllSales();

}
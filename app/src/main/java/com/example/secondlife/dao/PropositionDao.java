package com.example.secondlife.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.secondlife.models.City;
import com.example.secondlife.models.Proposition;

import java.util.List;

@Dao
public interface PropositionDao {

    @Query("SELECT * FROM proposition")
    LiveData<List<Proposition>> getAll();

    @Query("SELECT * FROM proposition WHERE propositionId = :id ")
    Proposition findById(int id);

    @Insert
    void insertAll(Proposition... propositions);

    @Insert
    void Insert(Proposition proposition);

    @Update
        //(onConflict = OnConflictStrategy.REPLACE)
    void Update(Proposition proposition);

    @Delete
    void Delete(Proposition proposition);

    @Query("DELETE FROM PROPOSITION")
    void DeleteAllPropositions();

}

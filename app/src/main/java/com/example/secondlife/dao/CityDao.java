package com.example.secondlife.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.secondlife.models.City;

import java.util.List;

@Dao
public interface CityDao {

    @Query("SELECT * FROM city")
    LiveData<List<City>> getAll();

    @Query("SELECT * FROM city WHERE id = :id ")
    City findById(int id);

    @Query("SELECT * FROM city WHERE cityName LIKE :cityName LIMIT 1")
    City findByName(String cityName);

    @Insert
    void insertAll(City... cities);

    @Insert
    void Insert(City city);

    @Update
        //(onConflict = OnConflictStrategy.REPLACE)
    void Update(City city);

    @Delete
    void Delete(City city);

    @Query("DELETE FROM CITY")
    void DeleteAllCities();

}

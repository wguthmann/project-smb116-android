package com.example.secondlife.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.secondlife.models.City;
import com.example.secondlife.models.Product;

import java.util.List;

@Dao
public interface ProductDao {

    @Query("SELECT * FROM product")
    LiveData<List<Product>> getAll();

    @Query("SELECT * FROM product WHERE productId = :id ")
    Product findById(int id);

    @Query("SELECT * FROM product WHERE title LIKE :title LIMIT 1")
    Product findByTitle(String title);

    @Insert
    void insertAll(Product... products);

    @Insert
    void Insert(Product product);

    @Update
        //(onConflict = OnConflictStrategy.REPLACE)
    void Update(Product product);

    @Query("DELETE FROM CITY")
    void DeleteAllProducts();

    @Delete
    void Delete(Product product);

}

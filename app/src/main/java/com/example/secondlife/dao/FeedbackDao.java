package com.example.secondlife.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.secondlife.models.City;
import com.example.secondlife.models.Feedback;

import java.util.List;

@Dao
public interface FeedbackDao {

    @Query("SELECT * FROM feedback")
    LiveData<List<Feedback>> getAll();

    @Query("SELECT * FROM feedback WHERE feedbackId = :id ")
    Feedback findById(int id);

    /*
    @Query("SELECT * FROM feedback WHERE raterUser LIKE :raterUser LIMIT 1")
    Feedback findByRaterUser(String raterUser);
    */
    @Insert
    void insertAll(Feedback... feedbacks);

    @Insert
    void Insert(Feedback feedback);

    @Update
        //(onConflict = OnConflictStrategy.REPLACE)
    void Update(Feedback feedback);

    @Query("DELETE FROM FEEDBACK")
    void DeleteAllFeedbacks();

    @Delete
    void Delete(Feedback feedback);

}

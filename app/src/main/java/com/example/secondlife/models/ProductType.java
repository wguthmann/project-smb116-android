package com.example.secondlife.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
public class ProductType implements Serializable {

    @PrimaryKey
    public int id;
    public String Title;
    public boolean PhysicalProduct;
    public int Rating ;

    public ProductType(){

    }

    public ProductType(int id, String title, boolean physicalProduct, int rating, Date releaseDate) {
        Title = title;
        PhysicalProduct = physicalProduct;
        Rating = rating;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public boolean isPhysicalProduct() {
        return PhysicalProduct;
    }

    public void setPhysicalProduct(boolean physicalProduct) {
        PhysicalProduct = physicalProduct;
    }

    public int getRating() {
        return Rating;
    }

    public void setRating(int rating) {
        Rating = rating;
    }

}
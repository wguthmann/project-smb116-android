package com.example.secondlife.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity
public class ProductGender {
    @PrimaryKey
    public int productGenderId;

    @ColumnInfo(name = "title")
    public String title;

    @ColumnInfo(name = "archivedAt")
    public Date archivedAt;

    public ProductGender(String title, Date archivedAt) {
        this.title = title;
        this.archivedAt = archivedAt;
    }

    public ProductGender(int productGenderId, String title, Date archivedAt) {
        this.productGenderId = productGenderId;
        this.title = title;
        this.archivedAt = archivedAt;
    }

    public int getProductGenderId() {
        return productGenderId;
    }

    public void setProductGenderId(int productGenderId) {
        this.productGenderId = productGenderId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getArchivedAt() {
        return archivedAt;
    }

    public void setArchivedAt(Date archivedAt) {
        this.archivedAt = archivedAt;
    }
}

package com.example.secondlife.models;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.Date;

@Entity
public class Feedback implements Serializable {
    @PrimaryKey
    public int feedbackId;

    @ColumnInfo(name = "text")
    public String text;

    @ColumnInfo(name = "rate")
    public float rate;

    //@ColumnInfo(name = "raterUser")
    @Embedded(prefix = "raterUser_")
    public User raterUser;

    //@ColumnInfo(name = "targetUser")
    @Embedded(prefix = "targetUser_")
    public User targetUser;

    @ColumnInfo(name = "releaseDate")
    public Date releaseDate;

    public Feedback() {
    }

    public Feedback(String text, float rate, User raterUser, User targetUser) {
        this.text = text;
        this.rate = rate;
        this.raterUser = raterUser;
        this.targetUser = targetUser;
    }

    public Feedback(int feedbackId, String text, float rate, User raterUser, User targetUser, Date releaseDate) {
        this.feedbackId = feedbackId;
        this.text = text;
        this.rate = rate;
        this.raterUser = raterUser;
        this.targetUser = targetUser;
        this.releaseDate = releaseDate;
    }

    public int getFeedbackId() {
        return feedbackId;
    }

    public void setFeedbackId(int feedbackId) {
        this.feedbackId = feedbackId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public User getRaterUser() {
        return raterUser;
    }

    public void setRaterUser(User raterUser) {
        this.raterUser = raterUser;
    }

    public User getTargetUser() {
        return targetUser;
    }

    public void setTargetUser(User targetUser) {
        this.targetUser = targetUser;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }
}

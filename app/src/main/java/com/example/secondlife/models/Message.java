package com.example.secondlife.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.Date;

@Entity
public class Message implements Serializable {
    @PrimaryKey
    public int messageId;

    @ColumnInfo(name = "text")
    public String text;

    @ColumnInfo(name = "linkPicture")
    public String linkPicture;

    @ColumnInfo(name = "receivedAt")
    public Date receivedAt;

    @ColumnInfo(name = "sendAt")
    public Date sendAt;

    @ColumnInfo(name = "releaseDate")
    public Date releaseDate;

    @ColumnInfo(name = "viewAt")
    public Date viewAt;

    public Message() {
    }

    public Message(String text, String linkPicture, Date receivedAt, Date sendAt, Date viewAt) {
        this.text = text;
        this.linkPicture = linkPicture;
        this.receivedAt = receivedAt;
        this.sendAt = sendAt;
        this.viewAt = viewAt;
    }

    public Message(int messageId, String text, String linkPicture, Date receivedAt, Date sendAt, Date releaseDate, Date viewAt) {
        this.messageId = messageId;
        this.text = text;
        this.linkPicture = linkPicture;
        this.receivedAt = receivedAt;
        this.sendAt = sendAt;
        this.releaseDate = releaseDate;
        this.viewAt = viewAt;
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLinkPicture() {
        return linkPicture;
    }

    public void setLinkPicture(String linkPicture) {
        this.linkPicture = linkPicture;
    }

    public Date getReceivedAt() {
        return receivedAt;
    }

    public void setReceivedAt(Date receivedAt) {
        this.receivedAt = receivedAt;
    }

    public Date getSendAt() {
        return sendAt;
    }

    public void setSendAt(Date sendAt) {
        this.sendAt = sendAt;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Date getViewAt() {
        return viewAt;
    }

    public void setViewAt(Date viewAt) {
        this.viewAt = viewAt;
    }
}

package com.example.secondlife.models;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.Date;

@Entity
public class Proposition implements Serializable {
    @PrimaryKey
    public Integer propositionId;

    @ColumnInfo(name = "proposalPrice")
    public Float proposalPrice;

    //@ColumnInfo(name = "exchangeProduct")
    @Embedded(prefix = "exchangeProduct_")
    public Product exchangeProduct;

    @ColumnInfo(name = "accepted")
    public Boolean accepted;

    //@ColumnInfo(name = "proposerUser")

    @Embedded(prefix = "proposerUser_")
    public User proposerUser;

    @ColumnInfo(name = "archivedAt")
    public Date archivedAt;

    @ColumnInfo(name = "releaseDate")
    public Date releaseDate;

    public Proposition() {
    }

    public Proposition(Float proposalPrice, Product exchangeProduct, Boolean accepted, User proposerUser, Date archivedAt) {
        this.proposalPrice = proposalPrice;
        this.exchangeProduct = exchangeProduct;
        this.accepted = accepted;
        this.proposerUser = proposerUser;
        this.archivedAt = archivedAt;
    }

    public Proposition(Integer propositionId, Float proposalPrice, Product exchangeProduct, Boolean accepted, User proposerUser, Date archivedAt, Date releaseDate) {
        this.propositionId = propositionId;
        this.proposalPrice = proposalPrice;
        this.exchangeProduct = exchangeProduct;
        this.accepted = accepted;
        this.proposerUser = proposerUser;
        this.archivedAt = archivedAt;
        this.releaseDate = releaseDate;
    }

    public int getPropositionId() {
        return propositionId;
    }

    public void setPropositionId(Integer propositionId) {
        this.propositionId = propositionId;
    }

    public float getProposalPrice() {
        return proposalPrice;
    }

    public void setProposalPrice(Float proposalPrice) {
        this.proposalPrice = proposalPrice;
    }

    public Product getExchangeProduct() {
        return exchangeProduct;
    }

    public void setExchangeProduct(Product exchangeProduct) {
        this.exchangeProduct = exchangeProduct;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(Boolean accepted) {
        this.accepted = accepted;
    }

    public User getProposerUser() {
        return proposerUser;
    }

    public void setProposerUser(User proposerUser) {
        this.proposerUser = proposerUser;
    }

    public Date getArchivedAt() {
        return archivedAt;
    }

    public void setArchivedAt(Date archivedAt) {
        this.archivedAt = archivedAt;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }
}

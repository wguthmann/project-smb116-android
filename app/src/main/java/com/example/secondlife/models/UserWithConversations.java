package com.example.secondlife.models;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Junction;
import androidx.room.Relation;

import java.io.Serializable;
import java.util.List;

@Entity
public class UserWithConversations implements Serializable {

    @Embedded
    public User user;

    @Relation(parentColumn = "userId", entityColumn = "ConversationId", associateBy = @Junction(UserConversationCrossRef.class))
    public List<Conversation> conversations;
}
package com.example.secondlife.models;

//import androidx.room.ColumnInfo;
//import androidx.room.Embedded;
//import androidx.room.Entity;
//import androidx.room.PrimaryKey;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

@Entity
public class User {

//    @PrimaryKey
//    @ColumnInfo(name = "userID")
//    public int userId;
//
//    @ColumnInfo(name = "username")
//    public String username;
//
//    @ColumnInfo(name = "emailAddress")
//    public String emailAddress;
//
//    @ColumnInfo(name = "password")
//    public String password;
//
//    @ColumnInfo(name = "city")
//    @Embedded(prefix = "city_")
//    public City city;
//
//    //public List<Conversation> conversations;
//
//    @ColumnInfo(name = "birthdate")
//    public Date birthdate;
//
//    @ColumnInfo(name = "archivedAt")
//    public Date archivedAt;
//
//    @ColumnInfo(name = "releaseDate")
//    public Date releaseDate;
//
//
//    public User(int userId, String username, String emailAddress, String password, City city, Date birthdate, Date archivedAt, Date releaseDate) {
//        this.userId = userId;
//        this.username = username;
//        this.emailAddress = emailAddress;
//        this.password = password;
//        this.city = city;
//        this.birthdate = birthdate;
//        this.archivedAt = archivedAt;
//        this.releaseDate = releaseDate;
//
//        //this.conversations = null; // ajouter conversations dans les paramètres ?
//    }
//
//    public int getUserId() {
//        return userId;
//    }
//    public void setUserId(int userId) {
//        this.userId = userId;
//    }
//
//    public String getUsername() {
//        return username;
//    }
//    public void setUsername(String username) {
//        this.username = username;
//    }
//
//    public String getEmailAddress() {
//        return emailAddress;
//    }
//    public void setEmailAddress(String emailAddress) {
//        this.emailAddress = emailAddress;
//    }
//
//    public String getPassword() {
//        return password;
//    }
//    public void setPassword(String password) {
//        this.password = password;
//    }
//
//    public City getCity() {
//        return city;
//    }
//    public void setCity(City city) {
//        this.city = city;
//    }
//
//    public Date getBirthdate() {
//        return birthdate;
//    }
//    public void setBirthdate(Date birthdate) {
//        this.birthdate = birthdate;
//    }
//
//    public Date getArchivedAt() {
//        return archivedAt;
//    }
//    public void setArchivedAt(Date archivedAt) {
//        this.archivedAt = archivedAt;
//    }

    // Livrable 1 : début
    @PrimaryKey
//    @ColumnInfo(name = "userID")
    @SerializedName("id")
    public Integer userId;

    //    @ColumnInfo(name = "username")
    @SerializedName("Username")
    public String username;

    //    @ColumnInfo(name = "EmailAddress")
    @SerializedName("emailAddress")
    public String emailAddress;

    //    @ColumnInfo(name = "password")
    @SerializedName("Password")
    public String password;

    //    @ColumnInfo(name = "city")
    @SerializedName("CityId")
    public String cityId;

    //    @ColumnInfo(name = "birthdate")
    @SerializedName("birthdate")
    public Date birthdate;


//    public User(String username, String emailAddress, String password, String city, Date birthdate) {
//        this.username = username;
//        this.emailAddress = emailAddress;
//        this.password = password;
//        this.city = city;
//        this.birthdate = birthdate;
//    }

//    public User(String username, String emailAddress, String password, String cityId) {
//        this.username = username;
//        this.emailAddress = emailAddress;
//        this.password = password;
//        this.cityId = cityId;
//    }


    public User(String username, String emailAddress, String password) {
        this.username = username;
        this.emailAddress = emailAddress;
        this.password = password;
    }

    @Ignore
    public User(String password, String username) {
        this.password = password;
        this.username = username;
    }

    public User() {
    }

    @Ignore


    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCity() {
        return cityId;
    }

    public void setCity(String city) {
        this.cityId = city;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }
    // Livrable 1 : fin
}
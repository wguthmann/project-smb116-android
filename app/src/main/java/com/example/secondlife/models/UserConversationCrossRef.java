package com.example.secondlife.models;

import androidx.room.Entity;

import java.io.Serializable;

@Entity(primaryKeys = {"userId", "conversationId"})
public class UserConversationCrossRef implements Serializable {
    public long userId;
    public long conversationId;
}

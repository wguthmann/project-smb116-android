package com.example.secondlife.models;

import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

import java.io.Serializable;
import java.util.List;

public class ConversationWithUsers implements Serializable {

    @Embedded
    public Conversation conversation;

    @Relation(parentColumn = "conversationId", entityColumn = "UserId", associateBy = @Junction(UserConversationCrossRef.class))
    public List<User> users;
}

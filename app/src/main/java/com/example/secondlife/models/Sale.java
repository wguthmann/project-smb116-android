package com.example.secondlife.models;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.Date;

@Entity
public class Sale implements Serializable {

    @PrimaryKey
    public int id;
    public float price;

    @Embedded(prefix = "buyer_")
    public User buyer;

    @Embedded(prefix = "seller_")
    public User seller;

    @Embedded(prefix = "saleProduct_")
    public Product saleProduct;

    public void Sale(){

    }

    public Sale(float price, User buyer, User seller, Product saleProduct) {
        this.price = price;
        this.buyer = buyer;
        this.seller = seller;
        this.saleProduct = saleProduct;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public User getBuyer() {
        return buyer;
    }

    public void setBuyer(User buyer) {
        this.buyer = buyer;
    }

    public User getSeller() {
        return seller;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }

    public Product getSaleProduct() {
        return saleProduct;
    }

    public void setSaleProduct(Product saleProduct) {
        this.saleProduct = saleProduct;
    }

}

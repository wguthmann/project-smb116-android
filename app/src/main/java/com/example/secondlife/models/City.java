package com.example.secondlife.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

@Entity
public class City implements Serializable {
    @PrimaryKey
    @SerializedName("id")
    public int id;

    @ColumnInfo(name = "cityName")
    @SerializedName("cityName")
    public String cityName;

    @ColumnInfo(name = "country")
    @SerializedName("country")
    public String country;

//    @ColumnInfo(name = "addedDate")
//    @SerializedName("addedDate")
//    public Date addedDate;

    public City() {
    }

    public City(String cityName, String country) {
        this.cityName = cityName;
        this.country = country;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

//    public Date getAddedDate() {
//        return addedDate;
//    }
//
//    public void setAddedDate(Date addedDate) {
//        this.addedDate = addedDate;
//    }
}

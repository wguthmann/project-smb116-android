package com.example.secondlife.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.Relation;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
public class Conversation implements Serializable {
    @PrimaryKey
    public int conversationId;

    //voir UserConversationCrossRef
    //@Relation(parentColumn = "id", entity = User.class, entityColumn = "ConversationId")
    //public List<User> users;

    @ColumnInfo(name = "releaseDate")
    public Date releaseDate;

    @ColumnInfo(name = "archivedAt")
    public Date archivedAt;

    public Conversation() {
    }

    public Conversation(Date archivedAt) {
        //this.users = users;
        this.archivedAt = archivedAt;
    }

    public Conversation(int conversationId, Date releaseDate, Date archivedAt) {
        this.conversationId = conversationId;
        this.releaseDate = releaseDate;
        this.archivedAt = archivedAt;
    }

    public int getConversationId() {
        return conversationId;
    }

    public void setConversationId(int conversationId) {
        this.conversationId = conversationId;
    }
/*
    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
*/
    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Date getArchivedAt() {
        return archivedAt;
    }

    public void setArchivedAt(Date archivedAt) {
        this.archivedAt = archivedAt;
    }
}

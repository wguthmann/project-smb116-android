package com.example.secondlife.models;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.Date;

@Entity
public class Product implements Serializable {
    @PrimaryKey
    public int productId;

    @ColumnInfo(name = "title")
    public String title;

    @ColumnInfo(name = "description")
    public String description;

    @ColumnInfo(name = "archivedAt")
    public Date archivedAt;

    @Embedded(prefix = "city_")
    public City city;

    //@ColumnInfo(name = "ownerUser")
    @Embedded(prefix = "ownerUser_")
    public User ownerUser;

    //@ColumnInfo(name = "productType")
    @Embedded(prefix = "productType_")
    public ProductType productType;

    @ColumnInfo(name = "releaseDate")
    public Date releaseDate;

    public Product() {
    }

    public Product(String title, String description, Date archivedAt, City city, User ownerUser, ProductType productType) {
        this.title = title;
        this.description = description;
        this.archivedAt = archivedAt;
        this.city = city;
        this.ownerUser = ownerUser;
        this.productType = productType;
    }

    public Product(int productId, String title, String description, Date archivedAt, City city, User ownerUser, ProductType productType, Date releaseDate) {
        this.productId = productId;
        this.title = title;
        this.description = description;
        this.archivedAt = archivedAt;
        this.city = city;
        this.ownerUser = ownerUser;
        this.productType = productType;
        this.releaseDate = releaseDate;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getArchivedAt() {
        return archivedAt;
    }

    public void setArchivedAt(Date archivedAt) {
        this.archivedAt = archivedAt;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public User getOwnerUser() {
        return ownerUser;
    }

    public void setOwnerUser(User ownerUser) {
        this.ownerUser = ownerUser;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }
}

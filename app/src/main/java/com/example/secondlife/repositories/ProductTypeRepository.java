package com.example.secondlife.repositories;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.secondlife.dao.ProductTypeDao;
import com.example.secondlife.database.SecondLifeDatabase;
import com.example.secondlife.models.ProductType;

import java.util.List;

public class ProductTypeRepository {

    private ProductTypeDao productTypeDao;
    private LiveData<List<ProductType>> allProductTypes;

    ProductTypeRepository(Application application) {
        SecondLifeDatabase db = SecondLifeDatabase.getInstance(application);
        productTypeDao = db.productTypeDao();
        allProductTypes = productTypeDao.getAll();
    }

    //methods for database operations :-
    public void insert(ProductType productType) {
        new InsertProductTypeAsyncTask(productTypeDao).execute(productType);
    }
    public void update(ProductType productType) {
        new UpdateProductTypeAsyncTask(productTypeDao).execute(productType);
    }
    public void delete(ProductType productType) {
        new DeleteProductTypeAsyncTask(productTypeDao).execute(productType);
    }
    public void deleteAllProductTypes() {
        new DeleteAllProductTypesAsyncTask(productTypeDao).execute();
    }
    public LiveData<List<ProductType>> getAllProductTypes() {
        return allProductTypes;
    }


    private static class InsertProductTypeAsyncTask extends AsyncTask<ProductType, Void, Void> {
        private ProductTypeDao productTypeDao;
        private InsertProductTypeAsyncTask(ProductTypeDao productTypeDao) {
            this.productTypeDao = productTypeDao;
        }
        @Override
        protected Void doInBackground(ProductType... productTypes) {
            productTypeDao.Insert(productTypes[0]); //single instance
            return null;
        }
    }
    private static class UpdateProductTypeAsyncTask extends AsyncTask<ProductType, Void, Void> {
        private ProductTypeDao productTypeDao;
        private UpdateProductTypeAsyncTask(ProductTypeDao productTypeDao) {
            this.productTypeDao = productTypeDao;
        }
        @Override
        protected Void doInBackground(ProductType... productTypes) {
            productTypeDao.Update(productTypes[0]);
            return null;
        }
    }
    private static class DeleteProductTypeAsyncTask extends AsyncTask<ProductType, Void, Void> {
        private ProductTypeDao productTypeDao;
        private DeleteProductTypeAsyncTask(ProductTypeDao productTypeDao) {
            this.productTypeDao = productTypeDao;
        }
        @Override
        protected Void doInBackground(ProductType... productTypes) {
            productTypeDao.Delete(productTypes[0]);
            return null;
        }
    }
    private static class DeleteAllProductTypesAsyncTask extends AsyncTask<Void, Void, Void> {
        private ProductTypeDao productTypeDao;
        private DeleteAllProductTypesAsyncTask(ProductTypeDao productTypeDao) {
            this.productTypeDao = productTypeDao;
        }
        @Override
        protected Void doInBackground(Void... voids) {
            productTypeDao.DeleteAllProductTypes();
            return null;
        }
    }
}

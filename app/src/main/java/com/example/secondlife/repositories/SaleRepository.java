package com.example.secondlife.repositories;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.secondlife.dao.SaleDao;
import com.example.secondlife.database.SecondLifeDatabase;
import com.example.secondlife.models.Sale;

import java.util.ArrayList;
import java.util.List;

public class SaleRepository {

    private SaleDao saleDao;
    private LiveData<List<Sale>> allSales;

    public SaleRepository(Application application) {
        SecondLifeDatabase db = SecondLifeDatabase.getInstance(application);
        saleDao = db.saleDao();
        allSales = saleDao.getAll();
    }

    //methods for database operations :-
    public void insert(Sale sale) {
        new InsertSaleAsyncTask(saleDao).execute(sale);
    }
    public void update(Sale sale) {
        new UpdateSaleAsyncTask(saleDao).execute(sale);
    }
    public void delete(Sale sale) {
        new DeleteSaleAsyncTask(saleDao).execute(sale);
    }
    public void deleteAllSales() {
        new DeleteAllSalesAsyncTask(saleDao).execute();
    }
    public LiveData<List<Sale>> getAllSales() {
        return allSales;
    }


    private static class InsertSaleAsyncTask extends AsyncTask<Sale, Void, Void> {
        private SaleDao saleDao;
        private InsertSaleAsyncTask(SaleDao saleDao) {
            this.saleDao = saleDao;
        }
        @Override
        protected Void doInBackground(Sale... sales) {
            saleDao.Insert(sales[0]); //single instance
            return null;
        }
    }
    private static class UpdateSaleAsyncTask extends AsyncTask<Sale, Void, Void> {
        private SaleDao saleDao;
        private UpdateSaleAsyncTask(SaleDao saleDao) {
            this.saleDao = saleDao;
        }
        @Override
        protected Void doInBackground(Sale... sales) {
            saleDao.Update(sales[0]);
            return null;
        }
    }
    private static class DeleteSaleAsyncTask extends AsyncTask<Sale, Void, Void> {
        private SaleDao saleDao;
        private DeleteSaleAsyncTask(SaleDao saleDao) {
            this.saleDao = saleDao;
        }
        @Override
        protected Void doInBackground(Sale... sales) {
            saleDao.Delete(sales[0]);
            return null;
        }
    }
    private static class DeleteAllSalesAsyncTask extends AsyncTask<Void, Void, Void> {
        private SaleDao saleDao;
        private DeleteAllSalesAsyncTask(SaleDao saleDao) {
            this.saleDao = saleDao;
        }
        @Override
        protected Void doInBackground(Void... voids) {
            saleDao.DeleteAllSales();
            return null;
        }
    }
}

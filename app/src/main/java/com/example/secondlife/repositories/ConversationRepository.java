package com.example.secondlife.repositories;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.secondlife.dao.CityDao;
import com.example.secondlife.dao.ConversationDao;
import com.example.secondlife.database.SecondLifeDatabase;
import com.example.secondlife.models.City;
import com.example.secondlife.models.Conversation;


import java.util.List;

public class ConversationRepository {

    private ConversationDao conversationDao;
    private LiveData<List<Conversation>> allConversations;

    ConversationRepository(Application application) {
        SecondLifeDatabase db = SecondLifeDatabase.getInstance(application);
        conversationDao = db.conversationDao();
        allConversations = conversationDao.getAll();
    }
    //methods for database operations :-
    public void insert(Conversation conversation) {
        new ConversationRepository.InsertConversationAsyncTask(conversationDao).execute(conversation);
    }
    public void update(Conversation conversation) {
        new ConversationRepository.UpdateConversationAsyncTask(conversationDao).execute(conversation);
    }
    public void delete(Conversation conversation) {
        new ConversationRepository.DeleteConversationAsyncTask(conversationDao).execute(conversation);
    }
    public void deleteAllConversations() {
        new ConversationRepository.DeleteAllConversationsAsyncTask(conversationDao).execute();
    }
    public LiveData<List<Conversation>> getAllConversations() {
        return allConversations;
    }


    private static class InsertConversationAsyncTask extends AsyncTask<Conversation, Void, Void> {
        private ConversationDao conversationDao;
        private InsertConversationAsyncTask(ConversationDao conversationDao) {
            this.conversationDao = conversationDao;
        }
        @Override
        protected Void doInBackground(Conversation... conversations) {
            conversationDao.Insert(conversations[0]); //single instance
            return null;
        }
    }
    private static class UpdateConversationAsyncTask extends AsyncTask<Conversation, Void, Void> {
        private ConversationDao conversationDao;
        private UpdateConversationAsyncTask(ConversationDao conversationDao) {
            this.conversationDao = conversationDao;
        }
        @Override
        protected Void doInBackground(Conversation... conversations) {
            conversationDao.Update(conversations[0]);
            return null;
        }
    }
    private static class DeleteConversationAsyncTask extends AsyncTask<Conversation, Void, Void> {
        private ConversationDao conversationDao;
        private DeleteConversationAsyncTask(ConversationDao conversationDao) {
            this.conversationDao = conversationDao;
        }
        @Override
        protected Void doInBackground(Conversation... conversations) {
            conversationDao.Delete(conversations[0]);
            return null;
        }
    }
    private static class DeleteAllConversationsAsyncTask extends AsyncTask<Void, Void, Void> {
        private ConversationDao conversationDao;
        private DeleteAllConversationsAsyncTask(ConversationDao conversationDao) {
            this.conversationDao = conversationDao;
        }
        @Override
        protected Void doInBackground(Void... voids) {
            conversationDao.DeleteAllConversations();
            return null;
        }
    }
}

package com.example.secondlife.repositories;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.secondlife.dao.PropositionDao;
import com.example.secondlife.database.SecondLifeDatabase;
import com.example.secondlife.models.Proposition;

import java.util.List;

public class PropositionRepository {

    private PropositionDao propositionDao;
    private LiveData<List<Proposition>> allPropositions;

    PropositionRepository(Application application) {
        SecondLifeDatabase db = SecondLifeDatabase.getInstance(application);
        propositionDao = db.propositionDao();
        allPropositions = propositionDao.getAll();
    }

    //methods for database operations :-
    public void insert(Proposition proposition) {
        new InsertPropositionAsyncTask(propositionDao).execute(proposition);
    }
    public void update(Proposition proposition) {
        new UpdatePropositionAsyncTask(propositionDao).execute(proposition);
    }
    public void delete(Proposition proposition) {
        new DeletePropositionAsyncTask(propositionDao).execute(proposition);
    }
    public void deleteAllPropositions() {
        new DeleteAllPropositionsAsyncTask(propositionDao).execute();
    }
    public LiveData<List<Proposition>> getAllPropositions() {
        return allPropositions;
    }


    private static class InsertPropositionAsyncTask extends AsyncTask<Proposition, Void, Void> {
        private PropositionDao propositionDao;
        private InsertPropositionAsyncTask(PropositionDao propositionDao) {
            this.propositionDao = propositionDao;
        }
        @Override
        protected Void doInBackground(Proposition... propositions) {
            propositionDao.Insert(propositions[0]); //single instance
            return null;
        }
    }
    private static class UpdatePropositionAsyncTask extends AsyncTask<Proposition, Void, Void> {
        private PropositionDao propositionDao;
        private UpdatePropositionAsyncTask(PropositionDao propositionDao) {
            this.propositionDao = propositionDao;
        }
        @Override
        protected Void doInBackground(Proposition... propositions) {
            propositionDao.Update(propositions[0]);
            return null;
        }
    }
    private static class DeletePropositionAsyncTask extends AsyncTask<Proposition, Void, Void> {
        private PropositionDao propositionDao;
        private DeletePropositionAsyncTask(PropositionDao propositionDao) {
            this.propositionDao = propositionDao;
        }
        @Override
        protected Void doInBackground(Proposition... propositions) {
            propositionDao.Delete(propositions[0]);
            return null;
        }
    }
    private static class DeleteAllPropositionsAsyncTask extends AsyncTask<Void, Void, Void> {
        private PropositionDao propositionDao;
        private DeleteAllPropositionsAsyncTask(PropositionDao propositionDao) {
            this.propositionDao = propositionDao;
        }
        @Override
        protected Void doInBackground(Void... voids) {
            propositionDao.DeleteAllPropositions();
            return null;
        }
    }
}

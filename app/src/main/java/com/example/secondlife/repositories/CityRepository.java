package com.example.secondlife.repositories;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.secondlife.dao.CityDao;
import com.example.secondlife.database.SecondLifeDatabase;
import com.example.secondlife.models.City;

import java.util.List;

public class CityRepository {

    private CityDao cityDao;
    private LiveData<List<City>> allCities;

    CityRepository(Application application) {
        SecondLifeDatabase db = SecondLifeDatabase.getInstance(application);
        cityDao = db.cityDao();
        allCities = cityDao.getAll();
    }

    //methods for database operations :-
    public void insert(City city) {
        new InsertCityAsyncTask(cityDao).execute(city);
    }
    public void update(City city) {
        new UpdateCityAsyncTask(cityDao).execute(city);
    }
    public void delete(City city) {
        new DeleteCityAsyncTask(cityDao).execute(city);
    }
    public void deleteAllCities() {
        new DeleteAllCitiesAsyncTask(cityDao).execute();
    }
    public LiveData<List<City>> getAllCities() {
        return allCities;
    }


    private static class InsertCityAsyncTask extends AsyncTask<City, Void, Void> {
        private CityDao cityDao;
        private InsertCityAsyncTask(CityDao cityDao) {
            this.cityDao = cityDao;
        }
        @Override
        protected Void doInBackground(City... cities) {
            cityDao.Insert(cities[0]); //single instance
            return null;
        }
    }
    private static class UpdateCityAsyncTask extends AsyncTask<City, Void, Void> {
        private CityDao cityDao;
        private UpdateCityAsyncTask(CityDao cityDao) {
            this.cityDao = cityDao;
        }
        @Override
        protected Void doInBackground(City... cities) {
            cityDao.Update(cities[0]);
            return null;
        }
    }
    private static class DeleteCityAsyncTask extends AsyncTask<City, Void, Void> {
        private CityDao cityDao;
        private DeleteCityAsyncTask(CityDao cityDao) {
            this.cityDao = cityDao;
        }
        @Override
        protected Void doInBackground(City... cities) {
            cityDao.Delete(cities[0]);
            return null;
        }
    }
    private static class DeleteAllCitiesAsyncTask extends AsyncTask<Void, Void, Void> {
        private CityDao cityDao;
        private DeleteAllCitiesAsyncTask(CityDao cityDao) {
            this.cityDao = cityDao;
        }
        @Override
        protected Void doInBackground(Void... voids) {
            cityDao.DeleteAllCities();
            return null;
        }
    }
}

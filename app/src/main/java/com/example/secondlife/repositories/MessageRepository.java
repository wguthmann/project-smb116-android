package com.example.secondlife.repositories;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.secondlife.dao.MessageDao;
import com.example.secondlife.database.SecondLifeDatabase;
import com.example.secondlife.models.Message;

import java.util.List;

public class MessageRepository {

    private MessageDao messageDao;
    private LiveData<List<Message>> allMessages;

    MessageRepository(Application application) {
        SecondLifeDatabase db = SecondLifeDatabase.getInstance(application);
        messageDao = db.messageDao();
        allMessages = messageDao.getAll();
    }

    //methods for database operations :-
    public void insert(Message message) {
        new InsertMessageAsyncTask(messageDao).execute(message);
    }
    public void update(Message message) {
        new UpdateMessageAsyncTask(messageDao).execute(message);
    }
    public void delete(Message message) {
        new DeleteMessageAsyncTask(messageDao).execute(message);
    }
    public void deleteAllMessages() {
        new DeleteAllMessagesAsyncTask(messageDao).execute();
    }
    public LiveData<List<Message>> getAllMessages() {
        return allMessages;
    }


    private static class InsertMessageAsyncTask extends AsyncTask<Message, Void, Void> {
        private MessageDao messageDao;
        private InsertMessageAsyncTask(MessageDao messageDao) {
            this.messageDao = messageDao;
        }
        @Override
        protected Void doInBackground(Message... messages) {
            messageDao.Insert(messages[0]); //single instance
            return null;
        }
    }
    private static class UpdateMessageAsyncTask extends AsyncTask<Message, Void, Void> {
        private MessageDao messageDao;
        private UpdateMessageAsyncTask(MessageDao messageDao) {
            this.messageDao = messageDao;
        }
        @Override
        protected Void doInBackground(Message... messages) {
            messageDao.Update(messages[0]);
            return null;
        }
    }
    private static class DeleteMessageAsyncTask extends AsyncTask<Message, Void, Void> {
        private MessageDao messageDao;
        private DeleteMessageAsyncTask(MessageDao messageDao) {
            this.messageDao = messageDao;
        }
        @Override
        protected Void doInBackground(Message... messages) {
            messageDao.Delete(messages[0]);
            return null;
        }
    }
    private static class DeleteAllMessagesAsyncTask extends AsyncTask<Void, Void, Void> {
        private MessageDao messageDao;
        private DeleteAllMessagesAsyncTask(MessageDao messageDao) {
            this.messageDao = messageDao;
        }
        @Override
        protected Void doInBackground(Void... voids) {
            messageDao.DeleteAllMessages();
            return null;
        }
    }
}

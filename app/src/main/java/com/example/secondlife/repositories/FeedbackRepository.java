package com.example.secondlife.repositories;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.secondlife.dao.FeedbackDao;
import com.example.secondlife.database.SecondLifeDatabase;
import com.example.secondlife.models.Feedback;

import java.util.List;

public class FeedbackRepository {

    private FeedbackDao feedbackDao;
    private LiveData<List<Feedback>> allFeedbacks;

    FeedbackRepository(Application application) {
        SecondLifeDatabase db = SecondLifeDatabase.getInstance(application);
        feedbackDao = db.feedbackDao();
        allFeedbacks = feedbackDao.getAll();
    }

    //methods for database operations :-
    public void insert(Feedback feedback) {
        new InsertFeedbackAsyncTask(feedbackDao).execute(feedback);
    }
    public void update(Feedback feedback) {
        new UpdateFeedbackAsyncTask(feedbackDao).execute(feedback);
    }
    public void delete(Feedback feedback) {
        new DeleteFeedbackAsyncTask(feedbackDao).execute(feedback);
    }
    public void deleteAllFeedbacks() {
        new DeleteAllFeedbacksAsyncTask(feedbackDao).execute();
    }
    public LiveData<List<Feedback>> getAllFeedbacks() {
        return allFeedbacks;
    }


    private static class InsertFeedbackAsyncTask extends AsyncTask<Feedback, Void, Void> {
        private FeedbackDao feedbackDao;
        private InsertFeedbackAsyncTask(FeedbackDao feedbackDao) {
            this.feedbackDao = feedbackDao;
        }
        @Override
        protected Void doInBackground(Feedback... feedbacks) {
            feedbackDao.Insert(feedbacks[0]); //single instance
            return null;
        }
    }
    private static class UpdateFeedbackAsyncTask extends AsyncTask<Feedback, Void, Void> {
        private FeedbackDao feedbackDao;
        private UpdateFeedbackAsyncTask(FeedbackDao feedbackDao) {
            this.feedbackDao = feedbackDao;
        }
        @Override
        protected Void doInBackground(Feedback... feedbacks) {
            feedbackDao.Update(feedbacks[0]);
            return null;
        }
    }
    private static class DeleteFeedbackAsyncTask extends AsyncTask<Feedback, Void, Void> {
        private FeedbackDao feedbackDao;
        private DeleteFeedbackAsyncTask(FeedbackDao feedbackDao) {
            this.feedbackDao = feedbackDao;
        }
        @Override
        protected Void doInBackground(Feedback... feedbacks) {
            feedbackDao.Delete(feedbacks[0]);
            return null;
        }
    }
    private static class DeleteAllFeedbacksAsyncTask extends AsyncTask<Void, Void, Void> {
        private FeedbackDao feedbackDao;
        private DeleteAllFeedbacksAsyncTask(FeedbackDao feedbackDao) {
            this.feedbackDao = feedbackDao;
        }
        @Override
        protected Void doInBackground(Void... voids) {
            feedbackDao.DeleteAllFeedbacks();
            return null;
        }
    }
}

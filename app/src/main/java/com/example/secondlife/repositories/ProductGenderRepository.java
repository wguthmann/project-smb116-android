package com.example.secondlife.repositories;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.secondlife.dao.ProductGenderDao;
import com.example.secondlife.database.SecondLifeDatabase;
import com.example.secondlife.models.ProductGender;

import java.util.List;

public class ProductGenderRepository {
    private ProductGenderDao productGenderDao;
    private LiveData<List<ProductGender>> allProductGenders;

    public ProductGenderRepository(Application application) {
        SecondLifeDatabase db = SecondLifeDatabase.getInstance(application);
        productGenderDao = db.productGenderDao();
//        allProductGenders = productGenderDao.getAll();
    }

    public void insert(ProductGender productGender) {
        new ProductGenderRepository.InsertProductGenderAsyncTask(productGenderDao).execute(productGender);
    }
    public void update(ProductGender productGender) {
        new ProductGenderRepository.UpdateProductGenderAsyncTask(productGenderDao).execute(productGender);
    }
    public void delete(ProductGender productGender) {
        new ProductGenderRepository.DeleteProductGenderAsyncTask(productGenderDao).execute(productGender);
    }
    public void deleteAllProducts() {
        new ProductGenderRepository.DeleteAllProductGendersAsyncTask(productGenderDao).execute();
    }
    public LiveData<List<ProductGender>> getAllProductGenders() {
        return allProductGenders;
    }
//    public ProductGender findOne(int id){
//        return productGenderDao.findById(id);
//    }

    private static class InsertProductGenderAsyncTask extends AsyncTask<ProductGender, Void, Void> {
        private ProductGenderDao productGenderDao;
        
        private InsertProductGenderAsyncTask(ProductGenderDao productGenderDao) {
            this.productGenderDao = productGenderDao;
        }
        
        @Override
        protected Void doInBackground(ProductGender... productGenders) {
//            productGenderDao.Insert(productGenders[0]); //single instance
            return null;
        }
    }

    private static class UpdateProductGenderAsyncTask extends AsyncTask<ProductGender, Void, Void> {
        private ProductGenderDao productGenderDao;
        private UpdateProductGenderAsyncTask(ProductGenderDao productGenderDao) {
            this.productGenderDao = productGenderDao;
        }
        @Override
        protected Void doInBackground(ProductGender... productGenders) {
//            productGenderDao.Update(productGenders[0]);
            return null;
        }
    }
    private static class DeleteProductGenderAsyncTask extends AsyncTask<ProductGender, Void, Void> {
        private ProductGenderDao productGenderDao;
        private DeleteProductGenderAsyncTask(ProductGenderDao productGenderDao) {
            this.productGenderDao = productGenderDao;
        }
        @Override
        protected Void doInBackground(ProductGender... productGenders) {
//            productGenderDao.Delete(productGenders[0]);
            return null;
        }
    }
    private static class DeleteAllProductGendersAsyncTask extends AsyncTask<Void, Void, Void> {
        private ProductGenderDao productGenderDao;
        private DeleteAllProductGendersAsyncTask(ProductGenderDao productGenderDao) {
            this.productGenderDao = productGenderDao;
        }
        @Override
        protected Void doInBackground(Void... voids) {
//            productGenderDao.DeleteAllProductGenders();
            return null;
        }
    }
}

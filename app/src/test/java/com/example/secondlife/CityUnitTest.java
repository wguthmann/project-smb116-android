package com.example.secondlife;

import com.example.secondlife.models.City;

import org.junit.Test;

import static org.junit.Assert.*;

public class CityUnitTest {

    City city = new City("Dublin");

    @Test
    public void assertThat_getName_givenDublin() {
        assertEquals("Dublin", city.getName());
    }
}
